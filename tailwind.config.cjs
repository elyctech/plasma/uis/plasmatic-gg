/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{html,component.ts}"],
  theme: {
    extend: {
      flex: {
        2: "2 2 0%",
      },
    },
  },
  plugins: [],
};
