import type { ValueProvider } from '@angular/core';
import {
  ActivatedRoute,
  ActivatedRouteSnapshot,
  type Data,
  NavigationEnd,
  Router,
} from '@angular/router';
import type { RenderComponentOptions } from '@testing-library/angular';
import { MockProvider, MockService } from 'ng-mocks';
import { EMPTY, Observable, of } from 'rxjs';

import { ParagraphComponent } from './app/elements/paragraph/paragraph.component';
import { ProjectService } from './app/services/project.service';
import { SessionService } from './app/services/session.service';

function mockActivatedRoute(
  data: Observable<Data> = EMPTY,
): [jasmine.SpyObj<ActivatedRoute>, ValueProvider, jasmine.Spy<jasmine.Func>] {
  const route = MockService<jasmine.SpyObj<ActivatedRoute>>(ActivatedRoute);

  // Apply data
  route.data = data;

  // Set-up snapshot
  const queryParamMapGetSpy = jasmine.createSpy(
    'route.snapshop.queryParamMap.get',
  );

  route.snapshot = MockService(ActivatedRouteSnapshot, {
    queryParamMap: {
      has: () => true,
      get: queryParamMapGetSpy,
      getAll: () => ['1'],
      keys: ['go'],
    },
  });

  return [
    route,
    MockProvider(ActivatedRoute, route, 'useValue'),
    queryParamMapGetSpy,
  ];
}

function mockProjectService(): [jasmine.SpyObj<ProjectService>, ValueProvider] {
  const projectService =
    MockService<jasmine.SpyObj<ProjectService>>(ProjectService);

  return [
    projectService,
    MockProvider(ProjectService, projectService, 'useValue'),
  ];
}

function mockRouter(): [jasmine.SpyObj<Router>, ValueProvider] {
  const router = MockService(Router, {
    events: of(
      new NavigationEnd(0, 'http://localhost:4200/', 'http://localhost:4200/'),
    ),
    serializeUrl: () => 'http://localhost:4200/',
  }) as jasmine.SpyObj<Router>;

  return [router, MockProvider(Router, router, 'useValue')];
}

function mockRoutingConfig<T>(): RenderComponentOptions<T> {
  return {
    imports: [ParagraphComponent],
    routes: [
      {
        path: '',
        component: ParagraphComponent,
      },
      {
        path: 'developer',
        component: ParagraphComponent,
      },
      {
        path: 'oops',
        component: ParagraphComponent,
      },
    ],
  };
}

function mockSessionService(): [jasmine.SpyObj<SessionService>, ValueProvider] {
  const sessionService =
    MockService<jasmine.SpyObj<SessionService>>(SessionService);

  return [
    sessionService,
    MockProvider(SessionService, sessionService, 'useValue'),
  ];
}

export {
  mockActivatedRoute,
  mockProjectService,
  mockRouter,
  mockRoutingConfig,
  mockSessionService,
};
