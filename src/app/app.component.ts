import { AsyncPipe, NgFor, NgIf } from '@angular/common';
import { Component, type OnDestroy } from '@angular/core';
import {
  ActivatedRoute,
  NavigationEnd,
  Router,
  RouterLink,
  RouterLinkActive,
  RouterOutlet,
} from '@angular/router';
import { Subscription, filter, map, mergeMap } from 'rxjs';

import { IconComponent, IconName } from './elements/icon/icon.component';
import { LogoBannerComponent } from './elements/logo-banner/logo-banner.component';
import { PageLayout } from './page-layout';

@Component({
  selector: 'app-root',
  standalone: true,
  templateUrl: './app.component.html',
  imports: [
    AsyncPipe,
    IconComponent,
    LogoBannerComponent,
    NgFor,
    NgIf,
    RouterLink,
    RouterLinkActive,
    RouterOutlet,
  ],
})
export class AppComponent implements OnDestroy {
  readonly NO_LAYOUT = PageLayout.None;
  readonly FOCUSED_LAYOUT = PageLayout.Focused;
  readonly FULL_LAYOUT = PageLayout.Full;

  readonly NAV_ITEMS = [
    {
      icon: IconName.Create,
      label: "Developer's Domain",
      route: '/developer',
    },
  ];

  layout: PageLayout;
  routerEventSubscription: Subscription;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
  ) {
    this.layout = PageLayout.Full;

    this.routerEventSubscription = this.router.events
      .pipe(
        filter((e) => e instanceof NavigationEnd),
        map(() => {
          let route = this.route;

          while (route.firstChild) {
            route = route.firstChild;
          }

          return route;
        }),
        mergeMap((route) => route.data),
        map((data) => (data['layout'] as PageLayout) ?? PageLayout.Full),
      )
      .subscribe((layout) => (this.layout = layout));
  }

  ngOnDestroy(): void {
    this.routerEventSubscription.unsubscribe();
  }
}
