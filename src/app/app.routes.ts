import { inject } from '@angular/core';
import type { Routes } from '@angular/router';

import { DeveloperDomainComponent } from './features/developer-domain/developer-domain.component';
import { hasSessionGuard } from './features/has-session.guard';
import { HomeComponent } from './features/home/home.component';
import { NewProjectComponent } from './features/new-project/new-project.component';
import { NotFoundComponent } from './features/not-found/not-found.component';
import { OopsComponent } from './features/oops/oops.component';
import { ProjectEditorComponent } from './features/project-editor/project-editor.component';
import { StartSessionComponent } from './features/start-session/start-session.component';
import { PageLayout } from './page-layout';
import { ProjectService } from './services/project.service';

export const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
    title: 'Plasma',
    data: {
      layout: PageLayout.None,
    },
  },
  {
    path: 'developer',
    canActivate: [hasSessionGuard],
    component: DeveloperDomainComponent,
    title: 'Developer Domain - Plasma',
  },
  {
    path: 'developer/project',
    canActivate: [hasSessionGuard],
    children: [
      {
        path: 'new',
        component: NewProjectComponent,
        title: 'New Project - Plasma',
      },
      {
        path: ':id',
        component: ProjectEditorComponent,
        title: async (route) => {
          const project = await inject(ProjectService).getProject(
            Number(route.params['id']),
          );

          return project ? `${project.name} - Plasma` : 'Plasma';
        },
        data: {
          layout: PageLayout.Focused,
        },
      },
    ],
  },
  {
    path: 'session',
    component: StartSessionComponent,
    title: 'Start Session - Plasma',
    data: {
      layout: PageLayout.None,
    },
  },
  {
    path: 'oops',
    component: OopsComponent,
    title: 'Oops! - Plasma',
    data: {
      layout: PageLayout.None,
    },
  },
  {
    path: '**',
    component: NotFoundComponent,
    title: '??? - Plasma',
    data: {
      layout: PageLayout.None,
    },
  },
];
