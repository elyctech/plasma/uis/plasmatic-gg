import { render, screen } from '@testing-library/angular';

import { mockProjectService } from '../../../test-helpers';
import { mockProject } from '../../mock-data/mock-projects';
import { ProjectEditorComponent } from './project-editor.component';

describe('ProjectEditorComponent', () => {
  it('renders properly', async () => {
    const [projectService, projectServiceProvider] = mockProjectService();

    projectService.getProject.and.resolveTo(mockProject);

    await render(ProjectEditorComponent, {
      componentProviders: [projectServiceProvider],
    });

    expect(screen.getByText(mockProject.name)).toBeInTheDocument();
  });
});
