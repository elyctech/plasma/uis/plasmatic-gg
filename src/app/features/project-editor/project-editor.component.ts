import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import {
  HeadingComponent,
  HeadingDisplay,
} from '../../elements/heading/heading.component';
import type { Project } from '../../services/project';
import { ProjectService } from '../../services/project.service';

@Component({
  selector: 'app-project-editor',
  standalone: true,
  templateUrl: './project-editor.component.html',
  imports: [HeadingComponent],
})
export class ProjectEditorComponent {
  readonly TITLE = HeadingDisplay.Title;

  project: Project;

  constructor(
    projectService: ProjectService,
    private route: ActivatedRoute,
  ) {
    this.project = {
      id: 0,
      name: '^UNKNOWN PROJECT^',
    };

    (async () => {
      const projectId = Number(this.route.snapshot.params['id']);
      const project = await projectService.getProject(projectId);

      if (project) {
        this.project = project;
      }
    })();
  }
}
