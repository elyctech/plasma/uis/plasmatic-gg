import { NgIf } from '@angular/common';
import { Component } from '@angular/core';
import { FormBuilder, ReactiveFormsModule, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { ButtonComponent } from '../../elements/button/button.component';
import { ContentBoxComponent } from '../../elements/content-box/content-box.component';
import {
  HeadingComponent,
  HeadingDisplay,
} from '../../elements/heading/heading.component';
import { LogoBannerComponent } from '../../elements/logo-banner/logo-banner.component';
import { ParagraphComponent } from '../../elements/paragraph/paragraph.component';
import { ServiceError } from '../../services/service-error';
import {
  SessionService,
  SessionServiceError,
} from '../../services/session.service';

@Component({
  selector: 'app-start-session',
  standalone: true,
  templateUrl: './start-session.component.html',
  imports: [
    ButtonComponent,
    ContentBoxComponent,
    HeadingComponent,
    LogoBannerComponent,
    NgIf,
    ParagraphComponent,
    ReactiveFormsModule,
  ],
})
export class StartSessionComponent {
  readonly TITLE = HeadingDisplay.Title;

  credentialsForm = this.formBuilder.nonNullable.group({
    username: ['', Validators.required],
    password: ['', Validators.required],
  });

  credentialsFormError: string | null = null;

  credentialsSubmitted = false;

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private sessionService: SessionService,
  ) {}

  passwordHasError(error: string): boolean {
    const passwordField = this.credentialsForm.get('password')!;

    return (
      (this.credentialsSubmitted || passwordField.touched) &&
      passwordField.errors?.[error]
    );
  }

  usernameHasError(error: string): boolean {
    const usernameField = this.credentialsForm.get('username')!;

    return (
      (this.credentialsSubmitted || usernameField.touched) &&
      usernameField.errors?.[error]
    );
  }

  async submitCredentials(): Promise<void> {
    this.credentialsSubmitted = true;

    if (this.credentialsForm.valid) {
      const { username, password } = this.credentialsForm.value;

      try {
        await this.sessionService.startSession(username!, password!);

        const go = this.route.snapshot.queryParamMap.get('go') ?? '/';
        await this.router.navigateByUrl(go);
      } catch (error) {
        if (error instanceof ServiceError) {
          switch (error.type) {
            case SessionServiceError.InvalidCredentials:
              this.credentialsFormError = 'Invalid Credentials';
              break;
            default:
              await this.router.navigateByUrl('/oops');
          }
        } else {
          await this.router.navigateByUrl('/oops');
        }
      }
    }
  }
}
