import {
  type RenderResult,
  render,
  screen,
  waitFor,
} from '@testing-library/angular';
import userEvent, { type UserEvent } from '@testing-library/user-event';

import {
  mockActivatedRoute,
  mockRouter,
  mockSessionService,
} from '../../../test-helpers';
import { mockSession } from '../../mock-data/mock-session';
import { ServiceError } from '../../services/service-error';
import { SessionServiceError } from '../../services/session.service';
import { StartSessionComponent } from './start-session.component';

interface ScenarioReturn {
  activatedRoute: ReturnType<typeof mockActivatedRoute>;
  router: ReturnType<typeof mockRouter>;
  sessionService: ReturnType<typeof mockSessionService>;

  user: UserEvent;

  mockArbitraryError(): void;
  mockGoPath(path: string): void;
  mockInvalidCredentialsResponse(): void;
  mockServiceUnavailable(): void;
  mockSuccessResponse(): void;

  provideUserCredentials(): Promise<void>;
}

function setUpScenario(): ScenarioReturn {
  const activatedRoute = mockActivatedRoute();
  const router = mockRouter();
  const sessionService = mockSessionService();

  const user = userEvent.setup();

  function mockGoPath(path: string): void {
    activatedRoute[2].and.returnValue(path);
  }

  function mockArbitraryError(): void {
    sessionService[0].startSession.and.rejectWith(new Error());
  }

  function mockInvalidCredentialsResponse(): void {
    sessionService[0].startSession.and.rejectWith(
      new ServiceError(SessionServiceError.InvalidCredentials),
    );
  }

  function mockServiceUnavailable(): void {
    sessionService[0].startSession.and.rejectWith(
      new ServiceError(SessionServiceError.Unavailable),
    );
  }

  function mockSuccessResponse(): void {
    sessionService[0].startSession.and.resolveTo(mockSession);
  }

  function renderRoute(): Promise<
    RenderResult<StartSessionComponent, StartSessionComponent>
  > {
    return render(StartSessionComponent, {
      componentProviders: [activatedRoute[1], router[1], sessionService[1]],
    });
  }

  async function provideUserCredentials(): Promise<void> {
    await renderRoute();

    await user.type(screen.getByLabelText('Username'), 'username');
    await user.type(screen.getByLabelText('Password'), 'password');
    await user.click(screen.getByText("Let's Go!"));
  }

  return {
    activatedRoute,
    mockArbitraryError,
    mockGoPath,
    mockInvalidCredentialsResponse,
    mockServiceUnavailable,
    mockSuccessResponse,
    provideUserCredentials,
    router,
    sessionService,
    user,
  };
}

describe('StartSessionComponent', () => {
  it('renders properly', async () => {
    await render(StartSessionComponent);

    expect(screen.getByText('Log In to Continue')).toBeInTheDocument();
  });

  it('reports a missing username properly', async () => {
    const user = userEvent.setup();
    await render(StartSessionComponent);

    await user.type(screen.getByLabelText('Password'), 'password');
    await user.click(screen.getByText("Let's Go!"));

    // TODO This is a purely visual check - both testing and the implementation
    // should be revisited with a11y in mind.
    expect(screen.getByText('Username is Required')).toHaveClass('opacity-100');
  });

  it('reports a missing password properly', async () => {
    const user = userEvent.setup();
    await render(StartSessionComponent);

    await user.type(screen.getByLabelText('Username'), 'username');
    await user.click(screen.getByText("Let's Go!"));

    // TODO This is a purely visual check - both testing and the implementation
    // should be revisited with a11y in mind.
    expect(screen.getByText('Password is Required')).toHaveClass('opacity-100');
  });

  it('allows submission when both username and password are present', async () => {
    const {
      sessionService: [sessionService],
      ...scenario
    } = await setUpScenario();

    scenario.mockSuccessResponse();
    await scenario.provideUserCredentials();

    // TODO Update when a mock server is present
    expect(sessionService.startSession).toHaveBeenCalled();
  });

  it('navigates to the given "go" path when the session is started', async () => {
    const {
      router: [router],
      ...scenario
    } = await setUpScenario();

    scenario.mockSuccessResponse();
    scenario.mockGoPath('/developer');

    await scenario.provideUserCredentials();

    expect(router.navigateByUrl).toHaveBeenCalledOnceWith('/developer');
  });

  it('navigates home if no "go" path is given when the session is started', async () => {
    const {
      router: [router],
      ...scenario
    } = await setUpScenario();

    scenario.mockSuccessResponse();

    await scenario.provideUserCredentials();

    expect(router.navigateByUrl).toHaveBeenCalledOnceWith('/');
  });

  it('reports when credentials are invalid', async () => {
    const scenario = await setUpScenario();

    scenario.mockInvalidCredentialsResponse();

    await scenario.provideUserCredentials();

    await waitFor(() => {
      expect(screen.getByText('Invalid Credentials')).toBeInTheDocument();
    });
  });

  it('navigates to the Oops page if the service is unavailable', async () => {
    const {
      router: [router],
      ...scenario
    } = await setUpScenario();

    scenario.mockServiceUnavailable();

    await scenario.provideUserCredentials();

    expect(router.navigateByUrl).toHaveBeenCalledOnceWith('/oops');
  });

  it('navigates to the Oops page if anything else unexpected goes wrong', async () => {
    const {
      router: [router],
      ...scenario
    } = await setUpScenario();

    scenario.mockArbitraryError();

    await scenario.provideUserCredentials();

    expect(router.navigateByUrl).toHaveBeenCalledOnceWith('/oops');
  });
});
