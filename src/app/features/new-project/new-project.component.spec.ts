import { fireEvent, render, screen } from '@testing-library/angular';

import { NewProjectComponent } from './new-project.component';

describe('NewProjectComponent', () => {
  it('renders properly', async () => {
    await render(NewProjectComponent);

    // TODO This implementation is not really finished, so for now just ensure nothing explodes
    const makeItButton = screen.getByText('Make It!');
    fireEvent.click(makeItButton);

    expect(makeItButton).toBeInTheDocument();
  });
});
