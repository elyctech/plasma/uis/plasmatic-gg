import { Component } from '@angular/core';
import { FormControl, FormGroup, ReactiveFormsModule } from '@angular/forms';
import { Router } from '@angular/router';

import { ButtonComponent } from '../../elements/button/button.component';
import { ProjectService } from '../../services/project.service';

@Component({
  selector: 'app-new-project',
  standalone: true,
  templateUrl: './new-project.component.html',
  imports: [ButtonComponent, ReactiveFormsModule],
})
export class NewProjectComponent {
  newProjectForm = new FormGroup({
    name: new FormControl('', {
      nonNullable: true,
    }),
  });

  constructor(
    private projectService: ProjectService,
    private router: Router,
  ) {}

  async createProject(): Promise<void> {
    const project = await this.projectService.createProject(
      this.newProjectForm.value.name!,
    );

    this.router.navigate(['/project', project.id]);
  }
}
