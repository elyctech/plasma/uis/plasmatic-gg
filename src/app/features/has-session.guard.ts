import { inject } from '@angular/core';
import { type CanActivateFn, Router } from '@angular/router';

import { SessionService } from '../services/session.service';

export const hasSessionGuard: CanActivateFn = (_, state) => {
  const router = inject(Router);
  const sessionService = inject(SessionService);

  if (!sessionService.hasActiveSession()) {
    return router.parseUrl(`/session?go=${state.url}`);
  }

  return true;
};
