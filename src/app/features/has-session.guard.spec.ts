import { TestBed } from '@angular/core/testing';
import {
  ActivatedRouteSnapshot,
  type CanActivateFn,
  RouterStateSnapshot,
  UrlTree,
} from '@angular/router';
import { MockProvider, MockService } from 'ng-mocks';

import { SessionService } from '../services/session.service';
import { hasSessionGuard } from './has-session.guard';

describe('hasSessionGuard', () => {
  const executeGuard: CanActivateFn = (...guardParameters) =>
    TestBed.runInInjectionContext(() => hasSessionGuard(...guardParameters));

  const route = MockService(ActivatedRouteSnapshot);
  const state = MockService(RouterStateSnapshot);

  let sessionService: jasmine.SpyObj<SessionService>;

  beforeEach(() => {
    sessionService =
      MockService<jasmine.SpyObj<SessionService>>(SessionService);

    TestBed.configureTestingModule({
      providers: [MockProvider(SessionService, sessionService, 'useValue')],
    });
  });

  it('allows a user with an active session', () => {
    sessionService.hasActiveSession.and.returnValue(true);

    expect(executeGuard(route, state)).toBeTrue();
  });

  it('redirects a user without an active session', () => {
    sessionService.hasActiveSession.and.returnValue(false);

    expect(executeGuard(route, state)).toBeInstanceOf(UrlTree);
  });
});
