import { NgFor } from '@angular/common';
import { Component } from '@angular/core';
import { RouterLink } from '@angular/router';

import {
  HeadingComponent,
  HeadingDisplay,
} from '../../elements/heading/heading.component';
import { ParagraphComponent } from '../../elements/paragraph/paragraph.component';

export interface HomePathOption {
  description: string;
  destination: string;
  title: string;
}

@Component({
  selector: 'app-home',
  standalone: true,
  templateUrl: './home.component.html',
  imports: [HeadingComponent, NgFor, ParagraphComponent, RouterLink],
})
export class HomeComponent {
  readonly HEADLINE = HeadingDisplay.Headline;
  readonly TITLE = HeadingDisplay.Title;

  readonly PATH_OPTIONS: HomePathOption[] = [
    {
      description: 'Explore what Plasma has to offer for your next game!',
      destination: '/developer',
      title: 'Developer',
    },
    {
      description: 'Discover great games to play on Plasma!',
      destination: '/player',
      title: 'Player',
    },
  ];
}
