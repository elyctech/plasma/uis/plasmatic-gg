import { render, screen } from '@testing-library/angular';

import { HomeComponent } from './home.component';

describe('HomeComponent', () => {
  it('renders properly', async () => {
    await render(HomeComponent);

    expect(screen.getByText('Welcome to Plasma!')).toBeInTheDocument();
    expect(screen.getByRole('link', { name: 'Developer' })).toBeInTheDocument();
    expect(screen.getByRole('link', { name: 'Player' })).toBeInTheDocument();
  });
});
