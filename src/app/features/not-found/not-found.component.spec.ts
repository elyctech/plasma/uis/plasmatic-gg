import { render, screen } from '@testing-library/angular';

import { NotFoundComponent } from './not-found.component';

describe('NotFoundComponent', () => {
  it('renders properly', async () => {
    await render(NotFoundComponent);

    expect(screen.getByText('Looking for Something?')).toBeInTheDocument();
  });
});
