import { Component } from '@angular/core';

import { ContentBoxComponent } from '../../elements/content-box/content-box.component';
import {
  HeadingComponent,
  HeadingDisplay,
} from '../../elements/heading/heading.component';
import { LogoBannerComponent } from '../../elements/logo-banner/logo-banner.component';
import { ParagraphComponent } from '../../elements/paragraph/paragraph.component';

@Component({
  selector: 'app-not-found',
  standalone: true,
  templateUrl: './not-found.component.html',
  imports: [
    ContentBoxComponent,
    HeadingComponent,
    LogoBannerComponent,
    ParagraphComponent,
  ],
})
export class NotFoundComponent {
  readonly TITLE = HeadingDisplay.Title;
}
