import { render, screen } from '@testing-library/angular';

import { mockProjectService } from '../../../test-helpers';
import { mockProjects } from '../../mock-data/mock-projects';
import { DeveloperDomainComponent } from './developer-domain.component';

describe('DeveloperDomainComponent', () => {
  it('renders properly', async () => {
    await render(DeveloperDomainComponent);

    expect(
      screen.getByText('Welcome fellow Developer!', { exact: false }),
    ).toBeInTheDocument();
  });

  it('renders the proper content when there are no projects', async () => {
    const [projectService, projectServiceProvider] = mockProjectService();

    projectService.getProjectsForCurrentUser.and.resolveTo([]);

    await render(DeveloperDomainComponent, {
      componentProviders: [projectServiceProvider],
    });

    expect(
      screen.getByText('Hmmm... it seems you do not have any projects (yet).'),
    ).toBeInTheDocument();
  });

  it('renders the proper content when there are projects', async () => {
    const [projectService, projectServiceProvider] = mockProjectService();

    projectService.getProjectsForCurrentUser.and.resolveTo(mockProjects);

    await render(DeveloperDomainComponent, {
      componentProviders: [projectServiceProvider],
    });

    expect(screen.getByText('Recent Projects')).toBeInTheDocument();
    expect(screen.getByText(mockProjects[0]!.name)).toBeInTheDocument();
    expect(screen.getByText(mockProjects[1]!.name)).toBeInTheDocument();
    expect(screen.getByText(mockProjects[2]!.name)).toBeInTheDocument();
  });
});
