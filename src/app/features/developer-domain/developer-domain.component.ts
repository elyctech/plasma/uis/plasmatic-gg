import { NgFor, NgIf } from '@angular/common';
import { Component } from '@angular/core';
import { RouterLink } from '@angular/router';

import { ButtonComponent } from '../../elements/button/button.component';
import { ContentBoxComponent } from '../../elements/content-box/content-box.component';
import {
  HeadingComponent,
  HeadingDisplay,
} from '../../elements/heading/heading.component';
import { IconName } from '../../elements/icon/icon.component';
import { ParagraphComponent } from '../../elements/paragraph/paragraph.component';
import type { Project } from '../../services/project';
import { ProjectService } from '../../services/project.service';

@Component({
  selector: 'app-developer-domain',
  standalone: true,
  templateUrl: './developer-domain.component.html',
  imports: [
    ButtonComponent,
    ContentBoxComponent,
    HeadingComponent,
    NgFor,
    NgIf,
    RouterLink,
    ParagraphComponent,
  ],
})
export class DeveloperDomainComponent {
  readonly PLUS_ICON = IconName.Plus;

  readonly HEADLINE = HeadingDisplay.Headline;
  readonly SUBTITLE = HeadingDisplay.Subtitle;
  readonly TITLE = HeadingDisplay.Title;

  // TODO Make this dynamic - perhaps based on presence of projects and/or last
  // time they were touched
  greeting = 'Welcome fellow Developer!';

  hasProjects = false;
  projects: Project[] = [];

  constructor(projectService: ProjectService) {
    (async () => {
      this.projects = await projectService.getProjectsForCurrentUser();
      this.hasProjects = this.projects.length > 0;
    })();
  }
}
