import { render, screen } from '@testing-library/angular';

import { OopsComponent } from './oops.component';

describe('OopsComponent', () => {
  it('renders properly', async () => {
    await render(OopsComponent);

    expect(screen.getByText('Well Then...')).toBeInTheDocument();
  });
});
