import { ActivatedRoute } from '@angular/router';
import {
  getByText,
  queryByText,
  render,
  screen,
} from '@testing-library/angular';
import { MockProvider, MockService } from 'ng-mocks';
import { of } from 'rxjs';

import {
  mockActivatedRoute,
  mockRouter,
  mockRoutingConfig,
} from '../test-helpers';
import { AppComponent } from './app.component';
import { PageLayout } from './page-layout';

describe('AppComponent', () => {
  it('renders a NO_LAYOUT route properly', async () => {
    const [_, activatedRouteProvider] = mockActivatedRoute(
      of({
        layout: PageLayout.None,
      }),
    );

    const [__, routerProvider] = mockRouter();

    await render(AppComponent, {
      componentProviders: [activatedRouteProvider, routerProvider],
    });

    const noLayoutContainer = screen.getByTestId('no-layout-container');

    expect(noLayoutContainer).toBeInTheDocument();
  });

  it('renders a FULL_LAYOUT route properly', async () => {
    const [_, activatedRouteProvider] = mockActivatedRoute(
      of({
        layout: PageLayout.Full,
      }),
    );

    const [__, routerProvider] = mockRouter();

    await render(AppComponent, {
      ...mockRoutingConfig(),
      componentProviders: [activatedRouteProvider, routerProvider],
    });

    const appLogoBanner = screen.getByTestId('app-logo-banner');
    const logoText = getByText(appLogoBanner, 'PLASMA');

    expect(logoText).toBeInTheDocument();
  });

  it('renders a FOCUSED_LAYOUT route properly', async () => {
    const [_, activatedRouteProvider] = mockActivatedRoute(
      of({
        layout: PageLayout.Focused,
      }),
    );

    const [__, routerProvider] = mockRouter();

    await render(AppComponent, {
      ...mockRoutingConfig(),
      componentProviders: [activatedRouteProvider, routerProvider],
    });

    const appLogoBanner = screen.getByTestId('app-logo-banner');
    const logoText = queryByText(appLogoBanner, 'PLASMA');

    expect(logoText).toBeNull();
  });

  it('renders a route without layout data as a FULL_LAYOUT', async () => {
    const [_, activatedRouteProvider] = mockActivatedRoute(of({}));

    const [__, routerProvider] = mockRouter();

    await render(AppComponent, {
      ...mockRoutingConfig(),
      componentProviders: [activatedRouteProvider, routerProvider],
    });

    const appLogoBanner = screen.getByTestId('app-logo-banner');
    const logoText = getByText(appLogoBanner, 'PLASMA');

    expect(logoText).toBeInTheDocument();
  });

  it('renders the proper layout for a nested route', async () => {
    // Wrap the route in a parent to test nested routes
    const [route, _] = mockActivatedRoute(
      of({
        layout: PageLayout.Focused,
      }),
    );

    const parentRoute = {
      firstChild: MockService(ActivatedRoute, route),
    };

    const activatedRouteProvider = MockProvider(ActivatedRoute, parentRoute);

    // Use the same ol' mock router for this test
    const [__, routerProvider] = mockRouter();

    await render(AppComponent, {
      componentProviders: [activatedRouteProvider, routerProvider],
    });

    const appLogoBanner = screen.getByTestId('app-logo-banner');
    const logoText = queryByText(appLogoBanner, 'PLASMA');

    expect(logoText).toBeNull();
  });
});
