import {
  Component,
  Input,
  type OnChanges,
  type OnInit,
  type SimpleChanges,
} from '@angular/core';

export enum IconName {
  Create = 1,
  Home,
  LeftArrow,
  Logo,
  Plus,
}

@Component({
  selector: 'app-icon',
  standalone: true,
  templateUrl: './icon.component.html',
  imports: [],
})
export class IconComponent implements OnChanges, OnInit {
  @Input() className = '';
  @Input() icon!: IconName;

  iconTarget = IconName[this.icon];

  ngOnChanges(changes: SimpleChanges): void {
    const newIcon = changes['icon'];

    if (newIcon) {
      this.iconTarget = IconName[newIcon.currentValue]!;
    }
  }

  ngOnInit() {
    this.iconTarget = IconName[this.icon];
  }
}
