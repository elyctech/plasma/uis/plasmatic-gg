import { render, screen } from '@testing-library/angular';

import { IconComponent, IconName } from './icon.component';

describe('IconComponent', () => {
  // TODO Test className?
  it('renders properly', async () => {
    const { rerender } = await render(IconComponent, {
      componentInputs: {
        icon: IconName.Home,
      },
    });

    let icon = screen.getByTestId('icon');

    expect(icon).toBeInTheDocument();
    expect(icon.style.maskImage).toContain('#Home');

    rerender({ componentInputs: { icon: IconName.Plus } });

    icon = screen.getByTestId('icon');

    expect(icon).toBeInTheDocument();
    expect(icon.style.maskImage).toContain('#Plus');
  });
});
