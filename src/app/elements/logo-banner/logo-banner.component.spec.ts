import { render, screen } from '@testing-library/angular';

import { LogoBannerComponent } from './logo-banner.component';

describe('LogoBannerComponent', () => {
  it('renders properly with no hideText input', async () => {
    await render(LogoBannerComponent);

    expect(screen.getByText('PLASMA')).toBeInTheDocument();
  });

  it('renders properly when hideText is false', async () => {
    await render(LogoBannerComponent, {
      componentInputs: {
        hideText: false,
      },
    });

    expect(screen.getByText('PLASMA')).toBeInTheDocument();
  });

  it('renders properly when hideText is true', async () => {
    await render(LogoBannerComponent, {
      componentInputs: {
        hideText: true,
      },
    });

    expect(screen.queryByText('PLASMA')).toBeNull();
  });
});
