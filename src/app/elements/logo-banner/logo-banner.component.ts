import { NgIf } from '@angular/common';
import { Component, Input } from '@angular/core';
import { RouterLink } from '@angular/router';

import { IconComponent, IconName } from '../icon/icon.component';

@Component({
  selector: 'app-logo-banner',
  standalone: true,
  templateUrl: './logo-banner.component.html',
  imports: [IconComponent, NgIf, RouterLink],
})
export class LogoBannerComponent {
  @Input() className = '';
  @Input() hideText = false;

  readonly LOGO_ICON = IconName.Logo;
}
