import { render, screen } from '@testing-library/angular';

import { ContentBoxComponent } from './content-box.component';

describe('ContentBoxComponent', () => {
  it('renders properly', async () => {
    await render(`<app-content-box>Premium Content</app-content-box>`, {
      imports: [ContentBoxComponent],
    });

    expect(screen.getByText('Premium Content')).toBeInTheDocument();
  });
});
