import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-content-box',
  standalone: true,
  templateUrl: './content-box.component.html',
  imports: [],
})
export class ContentBoxComponent {
  @Input() className = '';
}
