import { Component } from '@angular/core';

@Component({
  selector: 'app-paragraph',
  standalone: true,
  templateUrl: './paragraph.component.html',
  imports: [],
})
export class ParagraphComponent {}
