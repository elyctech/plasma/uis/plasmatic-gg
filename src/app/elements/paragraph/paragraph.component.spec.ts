import { render, screen } from '@testing-library/angular';

import { ParagraphComponent } from './paragraph.component';

describe('ParagraphComponent', () => {
  it('renders properly', async () => {
    await render(`<app-paragraph>Heyo!</app-paragraph>`, {
      imports: [ParagraphComponent],
    });

    expect(screen.getByText('Heyo!')).toBeInTheDocument();
  });
});
