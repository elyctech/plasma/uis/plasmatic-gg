import { NgIf } from '@angular/common';
import { Component, Input } from '@angular/core';

import { IconComponent, IconName } from '../icon/icon.component';

@Component({
  selector: 'app-button',
  standalone: true,
  templateUrl: './button.component.html',
  imports: [IconComponent, NgIf],
})
export class ButtonComponent {
  @Input() click = () => {};
  @Input() leadingIcon: IconName | null = null;
  @Input() text!: string;
  @Input() type = 'button';

  handleClick(): void {
    this.click();
  }
}
