import { fireEvent, render, screen } from '@testing-library/angular';

import { ButtonComponent } from './button.component';

describe('ButtonComponent', () => {
  // TODO Test other Inputs?
  it('renders properly', async () => {
    const { rerender } = await render(ButtonComponent, {
      componentInputs: {
        text: 'Heyo!',
      },
    });

    expect(screen.getByText('Heyo!')).toBeDefined();

    rerender({ componentInputs: { text: 'Whaddup!' } });

    expect(screen.getByText('Whaddup!')).toBeDefined();
  });

  it('does not explode if clicked without a click handler provided', async () => {
    await render(ButtonComponent, {
      componentInputs: {
        text: 'Heyo!',
      },
    });

    const button = screen.getByText('Heyo!');
    fireEvent.click(button);

    expect(button).toBeInTheDocument();
  });

  it('fires the click handler when clicked', async () => {
    const click = jasmine.createSpy();

    await render(ButtonComponent, {
      componentInputs: {
        click,
        text: 'Heyo!',
      },
    });

    fireEvent.click(screen.getByText('Heyo!'));

    expect(click).toHaveBeenCalled();
  });
});
