import { render, screen } from '@testing-library/angular';

import { HeadingComponent } from './heading.component';

describe('HeadingComponent', () => {
  it('renders properly', async () => {
    const { rerender } = await render(HeadingComponent, {
      componentInputs: {
        level: 1,
        text: 'Heyo!',
      },
    });

    expect(screen.getByText('Heyo!')).toBeInTheDocument();

    await rerender({ componentInputs: { level: 1, text: 'Whaddup!' } });

    expect(screen.getByText('Whaddup!')).toBeInTheDocument();
  });

  it('uses "h1" tag if level is 1', async () => {
    await render(HeadingComponent, {
      componentInputs: {
        level: 1,
        text: 'Heyo!',
      },
    });

    const element = screen.getByText('Heyo!');

    expect(element).toBeInTheDocument();
    expect(element.tagName).toBe('H1');
  });

  it('uses "h2" tag if level is 2', async () => {
    await render(HeadingComponent, {
      componentInputs: {
        level: 2,
        text: 'Heyo!',
      },
    });

    const element = screen.getByText('Heyo!');

    expect(element).toBeInTheDocument();
    expect(element.tagName).toBe('H2');
  });

  it('uses "h3" tag if level is 2', async () => {
    await render(HeadingComponent, {
      componentInputs: {
        level: 3,
        text: 'Heyo!',
      },
    });

    const element = screen.getByText('Heyo!');

    expect(element).toBeInTheDocument();
    expect(element.tagName).toBe('H3');
  });

  it('add the given "class"', async () => {
    await render(HeadingComponent, {
      componentInputs: {
        className: 'my css classes',
        level: 1,
        text: 'Heyo!',
      },
    });

    const element = screen.getByText('Heyo!');

    expect(element).toBeInTheDocument();
    expect(element).toHaveClass('my');
    expect(element).toHaveClass('css');
    expect(element).toHaveClass('classes');
  });
});
