import { NgIf } from '@angular/common';
import { Component, Input } from '@angular/core';

export enum HeadingDisplay {
  Headline = 'mb-3 text-5xl',
  Title = 'mb-1 text-4xl',
  Subtitle = 'mb-1 text-2xl',
}

@Component({
  selector: 'app-heading',
  standalone: true,
  templateUrl: './heading.component.html',
  imports: [NgIf],
})
export class HeadingComponent {
  @Input() className = '';
  @Input({ required: true }) display!: HeadingDisplay;
  @Input({ required: true }) level!: 1 | 2 | 3 | 4 | 5 | 6;
  @Input({ required: true }) text!: string;
}
