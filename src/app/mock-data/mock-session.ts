import type { UserSession } from '../services/user-session';

// TODO Move to test-helpers eventually
const mockSession: UserSession = {
  serviceUris: {
    session: 'somewhere',
  },
  token: 'therewasafarmerhadadogandbingowashisnameobingobingobingoandbingo',
  user: {
    displayName: 'Gahrayn',
    id: 42,
  },
};

export { mockSession };
