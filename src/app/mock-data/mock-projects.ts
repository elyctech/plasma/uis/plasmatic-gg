import type { Project } from '../services/project';

// TODO Move to test-helpers eventually
const mockProjects: Project[] = [
  {
    id: 13,
    name: 'Gaea Discovered',
  },
  {
    id: 42,
    name: 'Arpega',
  },
  {
    id: 151,
    name: 'Pokemon Epic',
  },
];

const mockProject = mockProjects[0]!;

export { mockProject, mockProjects };
