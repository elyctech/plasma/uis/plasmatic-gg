import { Injectable } from '@angular/core';

import { mockProjects } from '../mock-data/mock-projects';
import type { Project } from './project';

@Injectable({
  providedIn: 'root',
})
export class ProjectService {
  private projects: Project[] = mockProjects;

  createProject(name: string): Promise<Project> {
    const newProject = {
      id: 365,
      name,
    };

    this.projects.push(newProject);

    return Promise.resolve(newProject);
  }

  getProject(id: number): Promise<Project | null> {
    return Promise.resolve(
      this.projects.find((project) => project.id === id) ?? null,
    );
  }

  getProjectsForCurrentUser(): Promise<Project[]> {
    // return Promise.resolve(this.projects);
    return Promise.resolve([]);
  }
}
