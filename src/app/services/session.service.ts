import { Injectable } from '@angular/core';

import { mockSession } from '../mock-data/mock-session';
import { ServiceError } from './service-error';
import type { UserSession } from './user-session';

const START_SESSION_URI = 'http://localhost:3000/session';

export enum SessionServiceError {
  InvalidCredentials = '0:SessionServiceError',
  Unavailable = '1:SessionServiceError',
}

@Injectable({
  providedIn: 'root',
})
export class SessionService {
  private activeSession: UserSession | null = null;

  hasActiveSession(): boolean {
    return this.activeSession !== null;
  }

  getActiveSession(): UserSession | null {
    return this.activeSession;
  }

  async startSession(username: string, password: string): Promise<UserSession> {
    // if (password === 'a') {
    //   this.activeSession = mockSession;
    //   return mockSession;
    // } else if (password === 'b') {
    //   throw new ServiceError(SessionServiceError.InvalidCredentials);
    // }
    mockSession;

    try {
      const response = await fetch(START_SESSION_URI, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          username,
          password,
        }),
      });

      if (response.ok) {
        return response.json();
      } else {
        throw new ServiceError(SessionServiceError.InvalidCredentials);
      }
    } catch (e) {
      throw new ServiceError(SessionServiceError.Unavailable);
    }
  }
}
