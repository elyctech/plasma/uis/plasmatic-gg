import { TestBed } from '@angular/core/testing';

import { ProjectService } from './project.service';

// TODO This toooooo
describe('ProjectService', () => {
  let service: ProjectService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ProjectService);
  });

  it('does not explode', async () => {
    // This is all mock implementation right now, so there is not a lot of value in strictly testing it
    expect(service).toBeTruthy();
    expect(await service.createProject('Prahge')).toBeDefined();
    expect(await service.getProject(-1)).toBeDefined();
    expect(await service.getProject(13)).toBeDefined();
    expect(await service.getProjectsForCurrentUser()).toBeDefined();
  });
});
