export class ServiceError<T extends string> extends Error {
  constructor(public readonly type: T) {
    super(type);
  }
}
