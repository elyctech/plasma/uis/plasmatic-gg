import type { User } from './user';

export interface UserSession {
  serviceUris: {
    session: string;
  };
  token: string;
  user: User;
}
