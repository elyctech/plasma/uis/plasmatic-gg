import { TestBed } from '@angular/core/testing';

import { SessionService } from './session.service';

// TODO Thiiiis
describe('SessionService', () => {
  let service: SessionService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SessionService);
  });

  it('exists and junk', async () => {
    // TODO Test this when the real implementation is here.
    // This is all mock implementation right now, so there is not a lot of value in strictly testing it
    expect(service).toBeTruthy();
    expect(service.hasActiveSession()).toBeDefined();
    expect(service.getActiveSession()).toBeDefined();

    const fetch = spyOn(window, 'fetch');

    fetch.and.resolveTo(new Response('{}'));

    await expectAsync(service.startSession('', '')).toBeResolved();

    fetch.and.resolveTo(new Response(null, { status: 401 }));

    await expectAsync(service.startSession('', '')).toBeRejected();

    fetch.and.rejectWith(new TypeError('Failed to fetch'));

    await expectAsync(service.startSession('', '')).toBeRejected();
  });
});
