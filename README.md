# Plasmatic.gg

This is the code repo behind https://plasmatic.gg - the web app providing access to the capabilities of the Plasma gaming platform.

# Project Status

This project is in early development. In fact, its README is still a work-in-progress, if that was not already obvious.
